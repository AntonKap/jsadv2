"use strict";

let getDiv = document.querySelector("#root");

const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];

class User {
  constructor(author, name, price) {
    this.author = author;
    this.name = name;
    this.price = price;
  }
  renderBooks(newArr) {
    let newUl = document.createElement("ul");
    getDiv.append(newUl);
    let newLi = document.createElement("li");
    newLi.textContent = `author: ${this.author}`;
    newUl.append(newLi);
    newLi = document.createElement("li");
    newLi.textContent = `name: ${this.name}`;
    newUl.append(newLi);
    newLi = document.createElement("li");
    newLi.textContent = `price: ${this.price}`;
    newUl.append(newLi);
  }
}
let users = {}
function checkBook(arr) {
  for (let i = 0; i != arr.length; i++) {
    try {
      if (!arr[i].author || !arr[i].name || !arr[i].price) {
        throw new Error(`book ${i}. Object in arr with keys: ${Object.keys(arr[i])}, not full`);
      } else {
        users[`book ${i}`] = new User(arr[i].author, arr[i].name, arr[i].price);
        users[`book ${i}`].renderBooks([arr[i]]);
        // console.log( users[`book ${i}`] instanceof User);
      }
    } catch (e) {
      console.warn(e.message);
    }
  }
}
checkBook(books);
console.log(users);


